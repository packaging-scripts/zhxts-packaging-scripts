#!/bin/bash
set -e

# begin script
SCRIPT_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# For some insights into debian packaging:
# http://quietsche-entchen.de/cgi-bin/wiki.cgi/CreatingDebianPackages#M2
# http://linux.koolsolutions.com/2009/09/21/howto-create-your-own-local-debian-repository/

# input, you will only need to change these to create packages
if [ ! $# -eq 0 ]; then
    QT_GIT_DIR=$1
    ROOT_DIR=$2
    MKSPEC=$3
    ARCH=$4
    VERSION=$5
    BRANCH=$6
    DEVICE_NAME=$7
    XMKSPEC=$8
    SYSROOT=$9
else
    echo "Usage: $0 <QT4_GIT> <BUILD_ROOT> <PLATFORM_MKSPEC> <ARCH> <VESION_STRING> <GIT_BRANCH> [XPLATFORM_MKSPEC]"
    echo "    QT4_GIT must be suffixed with .git (e.g) ~/qt4-repo/.git"
    echo "    BUILD_ROOT is like the chroot for this script"
    exit
fi

source $SCRIPT_DIR/common.sh

if [ -z "$XMKSPEC" ]; then
    XMKSPEC=$MKSPEC
fi

if [ -z "$SYSROOT" ]; then
    SYSROOT="/"
fi

if [ $VERSION != "4.8" -a -z "$QT5BASE" ]; then
    echo "Qt 5 builds require QT5BASE to be exported correctly"
    exit
fi

XMKSPEC_STRING=`echo $XMKSPEC | sed -e 's/\//-/'`
SOURCE_DIR_BASE=$ROOT_DIR/source
SOURCE_DIR=$SOURCE_DIR_BASE/$PKG_PREFIX
NOW=$(date +%Y%m%d)
BUILD_DIR=$ROOT_DIR/build/$PKG_PREFIX$NICK
INSTALL_DIR=$ROOT_DIR/install/$PKG_PREFIX$NICK
LOG_PATH=$ROOT_DIR/logs
BUILD_NAME=$PKG_PREFIX-snapshot
INSTALL_PREFIX="/opt/$BUILD_NAME"
LOG_FILE=$LOG_PATH/$BUILD_NAME-$NOW-$XMKSPEC_STRING$NICK.log

#Qt 5
QT5_TARGET_DIR="$INSTALL_DIR$INSTALL_PREFIX"

# remove cruft when ^C
function cleanup() {
   rm -rf $INSTALL_PREFIX $SOURCE_DIR $INSTALL_DIR $BUILD_DIR
}

function control_c() {
   echo "Cleaning up..."
   #cleanup
   exit 1
}

trap control_c SIGINT

#cleanup
cleanup

mkdir -p $QT5_TARGET_DIR

# init logging
mkdir -p $LOG_PATH && echo "Track script status at " $LOG_FILE

rm -rf $SOURCE_DIR
GIT_SHA1=`GIT_DIR=$QT_GIT_DIR git rev-parse HEAD`

if [ $VERSION = "4.8" ]; then
    # extract latest source code
    mkdir -p $SOURCE_DIR
    GIT_DIR=$QT_GIT_DIR git archive --format=tar origin/$BRANCH 2>>$LOG_FILE | (cd $SOURCE_DIR && tar xf - >>$LOG_FILE 2>&1)
else
    mkdir -p $SOURCE_DIR_BASE
    ln -s $QT5BASE/qtbase $SOURCE_DIR
    STACKED_DIR=$PWD
    cd $QT5BASE
    git submodule | sed -e 's,^\(+\|-\), ,g' > "$QT5_TARGET_DIR/.submodule.sha1s"
    cd $STACKED_DIR
fi

# build Qt
exec 4>&2 3>&1 1>$LOG_FILE 2>&1 # redirect output temporarily to configure.log

#Oh so gross but feel free to fix it
if [ $VERSION = "4.8" ]; then
    mkdir -p $BUILD_DIR
    cd $BUILD_DIR

    if [ ! -x "$ARCH" ]; then
        ARCH_ARG="-arch $ARCH"
    fi
    $SOURCE_DIR/configure -opensource -confirm-license -no-qt3support -platform $MKSPEC -xplatform $XMKSPEC $ARCH_ARG -webkit -xmlpatterns \
                          -make libs -make tools -prefix $INSTALL_PREFIX -multimedia \
                          -force-pkg-config \
                          -javascript-jit -script -declarative -dbus -iconv -glib -opengl $CONFIGURE_FLAGS >>$LOG_FILE 2>&1

    make -j17
    # install Qt
    make INSTALL_ROOT=$INSTALL_DIR
else
    TRANSIENT_DIR="$SYSROOT$INSTALL_PREFIX"
    TOOLS_DIR=$INSTALL_PREFIX/bin

    rm -rf $TRANSIENT_DIR

#    echo "Setting PATH to $TOOLS_DIR:$PATH:$TOOLS_DIR"
#    export PATH=$TOOLS_DIR:$PATH:$TOOLS_DIR
#    export QTDIR=$INSTALL_PREFIX

#Remove until namespacing in webkit is known to work
#"-qtnamespace Qt5Snapshot -qtlibinfix Snapshot $CONFIGURE_FLAGS" \

    echo "Configure flags are $CONFIGURE_FLAGS"
    QT_BUILD_DIR=$BUILD_DIR PATH=$TOOLS_DIR:$PATH:$TOOLS_DIR $SCRIPT_DIR/build-qt5 \
        $INSTALL_PREFIX \
        "$CONFIGURE_FLAGS" \
        "" \
        "$ADDITIONAL_MODULES"

    mv $TRANSIENT_DIR/* $QT5_TARGET_DIR
fi

# restore stdout
exec 1>&3 2>&4 3>&- 4>&-

#if [ $VERSION != "4.8" ]; then
#    PATH=$PATH:$INSTALL_ROOT/bin/qmake
#fi

#    INSTALL_DIR=$1
#    VERSION=$2
#    ARCH=$3
#    LOG_FILE=$4
#    GIT_SHA1=$5
#    NOW=$6

$SCRIPT_DIR/package.sh $INSTALL_DIR $VERSION $ARCH $LOG_FILE $GIT_SHA1 $NOW $ROOT_DIR $XMKSPEC_STRING $DEVICE_NAME
