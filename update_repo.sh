INCOMING_DIR=~/incoming
INCOMING_DEVICE=$1
INCOMING_FILENAME=$2
INCOMING_FILE=$INCOMING_DIR/$INCOMING_FILENAME
DEB_ARCH=$3
MKSPEC=$4
REPO_DIR=/opt/archive.qmh-project.org/repo/$INCOMING_DEVICE/debian

# nuke old packages
rm -f $REPO_DIR/pool-$DEB_ARCH/*-$DEB_ARCH-$MKSPEC*deb

# copy new package
mv $INCOMING_FILE $REPO_DIR/pool-$DEB_ARCH

# update the Packages file
cd $REPO_DIR && dpkg-scanpackages pool-$DEB_ARCH /dev/null | gzip -9c > dists/unstable/main/binary-$DEB_ARCH/Packages.gz

echo "Update complete!"

exit # required for ssh command it seems

