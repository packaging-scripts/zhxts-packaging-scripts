#!/bin/bash

DIR=$(cd $(dirname $0) && pwd)
echo "Qt 4 builds"
echo "Starting package of 64-bit"
CONFIGURE_FLAGS="-webkit" $DIR/build_qt_package.sh ~/Qt/qt4/.git `pwd` linux-g++-64 x86_64 "4.8" "4.8" desktop
echo "Starting package of 32-bit"
CONFIGURE_FLAGS="-no-webkit" $DIR/build_qt_package.sh ~/Qt/qt4/.git `pwd` linux-g++-32 i386 "4.8" "4.8" desktop linux-g++-32
