#!/bin/bash
set -e

# begin script
SCRIPT_DIR="$( cd -P "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# For some insights into debian packaging:
# http://quietsche-entchen.de/cgi-bin/wiki.cgi/CreatingDebianPackages#M2
# http://linux.koolsolutions.com/2009/09/21/howto-create-your-own-local-debian-repository/

# input, you will only need to change these to create packages
if [ ! $# -eq 0 ]; then
    INSTALL_DIR=$1
    VERSION=$2
    ARCH=$3
    LOG_FILE=$4
    GIT_SHA1=$5
    NOW=$6
    ROOT_DIR=$7
    XMKSPEC_STRING=$8
    DEVICE_NAME=$9
else
    exit
fi

source $SCRIPT_DIR/common.sh

# derive debian arch
case "$ARCH" in
i386)
    DEB_ARCH=x86
    ;;
x86_64)
    DEB_ARCH=amd64
    ;;
armhf)
    DEB_ARCH=armhf
arm*)
    DEB_ARCH=armel
    ;;
*)
    echo "Unknown architecture"
    exit 1
esac

PACKAGE_DIR=$ROOT_DIR/packages
PACKAGE_FILENAME=$PKG_PREFIX-snapshot-$NOW-$DEB_ARCH-$XMKSPEC_STRING.deb
PACKAGE_NAME=$PACKAGE_DIR/$PACKAGE_FILENAME

# create the DEBIAN/ folder
mkdir -p $INSTALL_DIR/DEBIAN
md5sum `find . -type f | awk '/.\// { print substr($0, 3) }'` >$INSTALL_DIR/DEBIAN/md5sums
controlFileContents="Package: $PKG_PREFIX-snapshot
Version: $VERSION-snapshot$NOW
Section: libdevel
Maintainer: Girish Ramakrishnan <girish.1.ramakrishnan@nokia.com>
Priority: optional
Architecture: $DEB_ARCH
Description: Qt $VERSION daily snapshots
 Qt is a cross-platform C++ application framework. Qt's primary feature is
 a technology called QML, which couples a higher level Javascript language
 extension with a low level highly performant OpenGL scene graph.
 Historically Qt was known for its rich set of widgets that provide
 standard GUI functionality and provide the basis for the KDE libraries.
 .
 This package contains the header development files and libraries
 for testing against qt/$VERSION branch with the sha1
 $GIT_SHA1.
 .
 Untested and unsupported.
"

controlFileContents="${controlFileContents/NOW/$NOW}"
controlFileContents="${controlFileContents/DEB_ARCH/$DEB_ARCH}"
controlFileContents="${controlFileContents/GIT_SHA1/$GIT_SHA1}"
echo "$controlFileContents" >$INSTALL_DIR/DEBIAN/control
mkdir -p $PACKAGE_DIR

# build the package
dpkg-deb --build $INSTALL_DIR $PACKAGE_NAME >> $LOG_FILE

# and we are done
echo "Package ready at " $PACKAGE_NAME >> $LOG_FILE

# Update the repository
REPO_SERVER=qt@archive.qmh-project.org
REPO_DIR=/home/qt/incoming
scp $PACKAGE_NAME $REPO_SERVER:$REPO_DIR >> $LOG_FILE 2>&1
echo "Deploying with: ssh $REPO_SERVER /home/qt/packaging-scripts/update_repo.sh desktop $PACKAGE_FILENAME $DEB_ARCH $XMKSPEC_STRING" >> $LOG_FILE
ssh $REPO_SERVER /home/qt/packaging-scripts/update_repo.sh $DEVICE_NAME $PACKAGE_FILENAME $DEB_ARCH $XMKSPEC_STRING >> $LOG_FILE 2>&1 # basically does the lines below


# cd $REPO_DIR && dpkg-scanpackages binary-amd64 /dev/null | gzip -9c > dists/lenny/main/binary-amd64/Packages.gz
# cd $REPO_DIR && dpkg-scanpackages binary-i386 /dev/null | gzip -9c > dists/lenny/main/binary-i386/Packages.gz
