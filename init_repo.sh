REPOS=(desktop:amd64 desktop:i386 rpi:armel)
for REPO in "${REPOS[@]}"
do
        DEVICE=$(echo $REPO | cut -d":" -f1)
        ARCH=$(echo $REPO | cut -d":" -f2)

	REPO_DIR=/opt/archive.qmh-project.org/repo/$DEVICE/debian
	echo "Creating $REPO_DIR/pool-$ARCH"
        mkdir -p $REPO_DIR/pool-$ARCH
	echo "Creating $REPO_DIR/dists/unstable/main/binary-$ARCH"
	mkdir -p $REPO_DIR/dists/unstable/main/binary-$ARCH
	cd $REPO_DIR && dpkg-scanpackages pool-$ARCH /dev/null | gzip -9c > dists/unstable/main/binary-$ARCH/Packages.gz
done
