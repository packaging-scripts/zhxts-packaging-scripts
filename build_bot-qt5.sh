#!/bin/bash

DIR=$(cd $(dirname $0) && pwd)
if [ -z "$QT5BASE" ]; then
    echo "QT5BASE has to be exported before Qt 5 targets can be built"
    exit 101
fi

echo "Qt 5 builds"

#ADDITIONAL_MODULES="qtwayland" \
echo "Starting packaging of raspberry pi wheezy image"
PKG_CONFIG_LIBDIR= \
QT_WAYLAND_GL_CONFIG="brcm_egl" \
NICK="-wheezy" \
CONFIGURE_FLAGS="-opengl es2 -device linux-rasp-pi-g++ -device-option CROSS_COMPILE=/opt/toolchains/gcc-4.7-linaro-rpi-gnueabihf/bin/arm-linux-gnueabihf- -sysroot /mnt/rasp-pi-rootfs" \
$DIR/build_qt_package.sh $QT5BASE/qtbase/.git `pwd` linux-g++-32 armhf "5.0" "master" rpi-wheezy linux-rasp-pi-g++ "/mnt/rasp-pi-rootfs"

echo "Starting packaging of raspberry pi wheezy image"
#PKG_CONFIG_LIBDIR= \
#CONFIGURE_FLAGS="-opengl es2 -device linux-rasp-pi-g++ -device-option CROSS_COMPILE=/opt/toolchains/arm-2011.09/bin/arm-none-linux-gnueabi- -sysroot /mnt/rasp-pi-rootfs-legacy" \
#$DIR/build_qt_package.sh $QT5BASE/qtbase/.git `pwd` linux-g++-32 armel "5.0" "master" rpi linux-rasp-pi-g++ "/mnt/rasp-pi-rootfs-legacy"

echo "Starting packaging of 32-bit"
#$DIR/build_qt_package.sh $QT5BASE/qtbase/.git `pwd` linux-g++-32 i386 "5.0" "master" desktop linux-g++-32
echo "Starting packaging of clang build"
#$DIR/build_qt_package.sh $QT5BASE/qtbase/.git `pwd` "unsupported/linux-clang" x86_64 "5.0" desktop "master"
